# 12 Germinal

Valentine nous confie une mission dont le contenu est incertain.
Dans le Val de Lusemine, un personnage important a été porté disparu, et toutes les enquêtes ont menées à des disparitions inexpliquées, nous devons enquêter.
Valentine me remet aussi la propriété de mon domaine.

Nous partons sur place afin de tout préparer le plus vite possible.
Une fois sur place, nous retrouvons Roman et ses hommes. Nous nous installons tous dans le manoir après un état des lieux. Nous licencions la majorité des anciens employés, après avoir négocié avec "grand père feuillage" pour du matériel (bois vert, ébenite).

# 18 Germinal
Après avoir préparé notre attelage, deux golems et deux chariots spéciaux, nous partons vers notre nouvelle mission, laissant François et Roman gérer le manoir et faire des contrats de base.

Nous arrivons dans la ville de Vaillefrange. Nous rencontrons le capitaine de la garde, et on apprend de nombreux détails sur l'affaire.

# 19 Germinal
Nous allons discuter avec la Maire, le Paladin puis les magiciens du village.
Au total, nous trouvons:

- Il y a 45 ans, le village de Sai-Rae a été deserté car Vigrid a décidé de centraliser à cette époque la religion. C'était un lieu de Pélerinage connu pour être le lieu du combat et de la victoire des humains contre une créature. Nous déduisons que cette créature était celle que les humains et les nains ont vaincu il y a 3000 ans. Le village lui-même étant à quelques kilomètres au sud des montagnes naines.
- Il y a 2 mois, des voyageurs étranges sont passés en pélerinage vers le nord de Vaillefrange, surement vers Sai-Rae. Un ou deux village ont commencés à arrêter d'envoyer des nouvelles.
- Il y a 1 mois, Julius, un jeune paladin haut-gradé passe par Vaillefrange et disparait en allant au nord, accompagné d'une femme "sa soeur".
- À la même époque, un village est brûlé avec ses habitants par des hommes en armures dorées qui veulent "purger le monde des démons". Depuis ce temps, la région est couverte par la pluie.
- Il faut une grande puissance pour bloquer la magie de contrôle des golems, et une connaissance fine (donc du mechanicum) pour désactiver les golems.

Nous partons ensuite vers le village Cadun (celui qui a été brûlé).

# 20 Germinal

Nous somme réveillés par l'arrivée de 3 chevaliers en armure dorée. Ceux-là même parlent de jugement, et de chasser les démons. Nous les attaquons, tuons 2 d'entre-eux et en capturons un que nous interrogeons une fois rentré en ville.