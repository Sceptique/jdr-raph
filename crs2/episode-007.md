# 28 Ventôse
On essaie d'appliquer le plan.
On trouve que les fées sont en danger, attaquées par les golems.
On les détruit et on trouve Ana qui controle mentalement les fées.
On attaque et on finit par vaincre avec l'arrivée de nos compagnons mages et bardes au prix de nombreuses souffrances.

On amène Ana devant l'accusatrice et on repart ensuite avec quelques infos sur le cosmos.

Au manoir, Evan est arrêté par l'apprentie de Valentine. On l'accompagne après lui avoir donné les preuves.
On se retrouve avec Valentine qui nous dit qu'elle a donné mon nom au bonnes personnes et dans 2 semaines on aura le manoir et ses dépendances.

On trouve un livre important mais encore indéchiffré et Valentine nous donne un mouchard pour nos prochaines enquêtes.

**2 semaines passent**